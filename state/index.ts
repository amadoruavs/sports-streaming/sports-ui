import Vue from "vue";

import initialData from "./data";
import updaters from "./updaters";

const state = new Vue({
    data() {
        return initialData;
    },
    computed: {
        scoreIndicatorProps(): IScoreIndicatorProps {
            return {
                homeName: this.teams.Home.name,
                homeScore: this.teams.Home.score.toString(),
                homeColor: this.teams.Home.pColor,
                homePossession: this.possession === "Home",
                homeTimeouts: this.teams.Home.timeouts.toString(),

                awayName: this.teams.Away.name,
                awayScore: this.teams.Away.score.toString(),
                awayColor: this.teams.Away.pColor,
                awayPossession: this.possession === "Away",
                awayTimeouts: this.teams.Away.timeouts.toString(),

                quarter: `Q${this.gameClock.quarter}`,
                gameTime: this.formatTime(this.gameClock.time),
                playTime: this.playClock.time.toString(),

                down: this.down.toString(),
                distance: this.distance.toString(),
            };
        },
        scoreIndicatorRunning(): boolean {
            return this.templates.scoreIndicator.running;
        },
        lowerThirdRunning(): boolean {
            return this.templates.lowerThird.running;
        },
        creditsRunning(): boolean {
            return this.templates.credits.running;
        },
        liveSymbolRunning(): boolean {
            return this.templates.liveSymbol.running;
        },
        lowerThirdProps(): ILowerThirdProps {
            const pregame =
                this.gameClock.quarter === 1 && this.gameClock.time === 720;

            return {
                homeName: this.teams.Home.fullName,
                homeImagePath: `images/${this.teams.Home.name.toLowerCase()}.svg`,
                homeColor: this.teams.Home.pColor,
                homeScore: pregame ? "" : this.teams.Home.score.toString(),
                awayName: this.teams.Away.fullName,
                awayColor: this.teams.Away.pColor,
                awayImagePath: `images/${this.teams.Away.name.toLowerCase()}.svg`,
                awayScore: pregame ? "" : this.teams.Away.score.toString(),

                ltText: this.lowerThirdText.trim(),
            };
        },
    },
    watch: {
        scoreIndicatorProps() {
            updaters.updateScoreIndicator("UPDATE", this.scoreIndicatorProps);
        },
        scoreIndicatorRunning() {
            updaters.updateScoreIndicator(
                this.scoreIndicatorRunning ? "PLAY" : "STOP",
                this.scoreIndicatorProps
            );
        },
        lowerThirdRunning() {
            updaters.updateLowerThird(
                this.lowerThirdRunning ? "PLAY" : "STOP",
                this.lowerThirdProps
            );
        },
        lowerThirdProps() {
            updaters.updateLowerThird("UPDATE", this.lowerThirdProps);
        },
        creditsRunning() {
            updaters.toggleCredits(this.creditsRunning);
        },
        liveSymbolRunning() {
            updaters.toggleLiveSymbol(this.liveSymbolRunning);
        }
    },

    methods: {
        formatTime(duration: number) {
            const mins = ~~(duration / 60);
            const secs = ~~duration % 60;

            let ret = "";

            ret += mins + ":" + (secs < 10 ? "0" : "");
            ret += secs;
            return ret;
        },
        triggerTouchdown(team: TeamT) {
            updaters.triggerTouchdown(team, {
                color: this.teams[team].pColor,
                imagePath: `images/${this.teams[team].name.toLowerCase()}.svg`,
            });
        },
    },
});

if (process.env.NODE_ENV === "development") {
    //@ts-ignore
    window.__state = state;
}

export default state;
