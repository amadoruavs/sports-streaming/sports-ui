const data: IState = {
    gameStarted: false,

    down: "1st",
    distance: 10,
    gameClock: {
        quarter: 1,
        time: 720,
        running: false,
    },
    playClock: {
        type: 40, // 40 or 25
        time: 40,
        running: false,
    },
    possession: "Home",
    teams: {
        Home: {
            pColor: "#8A00A5",
            name: "Amador",
            fullName: "Amador Valley Dons",
            timeouts: 3,
            score: 0,
        },
        Away: {
            pColor: "#EC3247",
            name: "Dublin",
            fullName: "Dublin Gaels",
            timeouts: 3,
            score: 0,
        },
    },
    templates: {
        scoreIndicator: {
            running: false,
        },
        lowerThird: {
            running: false,
        },
        credits: {
            running: false
        },
        liveSymbol: {
            running: false
        }
    },
    lowerThirdText: "The game will begin shortly.",
};

export default data;