import axios from "axios";
import wait from "~/helpers/wait";

const updaters = {
    async updateLowerThird(command: CommandT, data?: ILowerThirdProps) {
        const body: IRawUpdateParams<ILowerThirdProps> = {
            command,
            templateName: "new-lower-third",
            data,
        };

        try {
            const res = await axios.post<IUpdateResponse>(
                `${process.env.ENDPOINT_URL}/updateData`,
                body
            );
            console.log("Updated lower-third", res);
        } catch (error) {
            console.log("Error updating lower-third", error.response);
        }
    },
    async toggleCredits(running: boolean) {
        const body: IRawUpdateParams = {
            templateName: "credits",
            command: running ? "PLAY" : "STOP",
        };

        try {
            const res = await axios.post<IUpdateResponse>(
                `${process.env.ENDPOINT_URL}/updateData`,
                body
            );
            console.log("Updated credits", res);
        } catch (error) {
            console.log("Error updating credits", error.response);
        }
    },
    async updateScoreIndicator(command: CommandT, props: IScoreIndicatorProps) {
        const body: IRawUpdateParams<IScoreIndicatorProps> = {
            templateName: "score-indicator",
            data: props,
            command: command,
        };

        try {
            const res = await axios.post<IUpdateResponse>(
                `${process.env.ENDPOINT_URL}/updateData`,
                body
            );

            console.log("Successfully updated score-indicator", res);
        } catch (error) {
            console.log("Error updating score-indicator", error.response);
        }
    },
    async toggleLiveSymbol(running: boolean) {
        const body: IRawUpdateParams = {
            templateName: "live-symbol",
            command: running ? "PLAY" : "STOP",
        };

        try {
            const res = await axios.post<IUpdateResponse>(
                `${process.env.ENDPOINT_URL}/updateData`,
                body
            );
            console.log("Updated live-symbol", res);
        } catch (error) {
            console.log("Error updating live-symbol", error.response);
        }
    },
    async triggerTouchdown(team: TeamT, data: ITouchdownProps) {
        const body: IRawUpdateParams<ITouchdownProps> = {
            command: "PLAY",
            templateName: `${team.toLowerCase()}-touchdown` as TemplateName,
            data,
        };

        try {
            const res = await axios.post<IUpdateResponse>(
                `${process.env.ENDPOINT_URL}/updateData`,
                body
            );

            await wait(2000);

            await axios.post(`${process.env.ENDPOINT_URL}/updateData`, {
                command: "STOP",
                templateName: `${team.toLowerCase()}-touchdown` as TemplateName,
            });

            console.log("Successfully updated touchdown", res);
        } catch (error) {
            console.log("Error updating touchdown", error.response);
        }
    },
};

export default updaters;
