import axios from "axios";
import state from "~/state";
import updaters from "~/state/updaters";


export default async function addInitialTemplates() {
    await updaters.updateScoreIndicator("ADD", state.scoreIndicatorProps);
    await updaters.updateLowerThird("ADD", state.lowerThirdProps);
}