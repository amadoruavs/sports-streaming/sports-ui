import axios from "axios";
import state from "~/state";

export default async function syncGameState() {
    const getResponse = await axios.get<IResponse<{ layers: ILayerSchema[] }>>(
        `${process.env.ENDPOINT_URL}/getData`
    );

    for (const layer of getResponse.data.data.layers) {
        console.log(getResponse.data.data);
        switch (layer.templateName) {
            case "score-indicator/index":
                state.templates.scoreIndicator.running = layer.playing;

                const data = (layer.data as unknown) as IScoreIndicatorProps;
                state.teams.Home.name = data.homeName;
                state.teams.Away.name = data.awayName;

                state.teams.Home.pColor = data.homeColor;
                state.teams.Away.pColor = data.awayColor;

                state.teams.Home.score = Number(data.homeScore);
                state.teams.Away.score = Number(data.awayScore);

                state.teams.Home.timeouts = Number(
                    data.homeTimeouts
                ) as TimeoutT;
                state.teams.Away.timeouts = Number(
                    data.awayTimeouts
                ) as TimeoutT;

                state.possession = data.homePossession ? "Home" : "Away";

                state.gameClock.quarter = Number(data.quarter[1]) as QuarterT;
                const [min, sec] = data.gameTime.trim().split(":");
                state.gameClock.time = Number(min) * 60 + Number(sec);

                state.playClock.time = Number(data.playTime);
                state.down = data.down as DownT;
                state.distance = Number(data.distance);
                break;
            case "new-lower-third/index":
                state.templates.lowerThird.running = layer.playing;

                state.teams.Home.fullName = layer.data.homeName as string;
                state.teams.Away.fullName = layer.data.awayName as string;
                state.lowerThirdText = layer.data.ltText as string;

            case "credits/index":
                state.templates.credits.running = layer.playing;
        }
    }
}
