declare interface IScoreIndicatorProps {
    homeName: string;
    homeColor: string;
    homePossession: boolean;
    homeTimeouts: string;
    homeScore: string;
    awayName: string;
    awayColor: string;
    awayPossession: boolean;
    awayTimeouts: string;
    awayScore: string;
    quarter: string;
    gameTime: string;
    playTime: string;
    down: string;
    distance: string;
}

declare interface ILowerThirdProps {
    homeScore: string;
    homeName: string;
    homeColor: string;
    homeImagePath: string;

    awayScore: string;
    awayName: string;
    awayColor: string;
    awayImagePath: string;

    ltText: string;
}

declare interface ITouchdownProps {
    color: string;
    imagePath: string;
}
