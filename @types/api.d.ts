declare type TemplateName =
    | "away-touchdown"
    | "credits"
    | "home-touchdown"
    | "score-indicator"
    | "new-lower-third"
    | "live-symbol";

declare type CommandT = "ADD" | "PLAY" | "STOP" | "UPDATE" | "REMOVE";

declare interface IRawUpdateParams<Data=undefined> {
    templateName: TemplateName; // the template name you want to change
    command: CommandT; // action
    data?: Data; // data payload (optional)
    autoPlay?: boolean; // autoplay (optional, default = false)
}

declare interface ILayerSchema {
    layer: number;
    templateName: string;
    data: Record<string, unknown>;
    playing: boolean;
}

declare interface IResponse<Data = null> {
    success: boolean; // success lol
    data: Data; // detailed response or null
    error: string | null; // error message or null
    code: number; // status code
}

declare interface IUpdateResponse {
    success: boolean; // success lol
    data: { message: string; payload: object } | null; // detailed response or null
    error: string | null; // error message or null
    code: number; // status code
}