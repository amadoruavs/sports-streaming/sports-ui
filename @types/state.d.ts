declare interface ITemplate {
    running: boolean;
}

declare interface ITeam {
    pColor: string;
    name: string;
    fullName: string;
    score: number;
    timeouts: TimeoutT;
}

declare type TimeoutT = 3 | 2 | 1 | 0;
declare type TemplateT = "scoreIndicator" | "lowerThird" | "credits" | "liveSymbol";
declare type TeamT = "Home" | "Away";
declare type QuarterT = 1 | 2 | 3 | 4;
declare type DownT = "1st" | "2nd" | "3rd" | "4th";
declare type PlayClockT = 40 | 25;

declare interface IState {
    gameStarted: boolean,

    down: DownT;
    distance: number;
    gameClock: ITemplate & {
        quarter: QuarterT;
        time: number;
    };
    playClock: ITemplate & {
        type: PlayClockT;
        time: number;
        running: boolean;
    };
    possession: TeamT;
    teams: Record<TeamT, ITeam>;
    templates: Record<TemplateT, ITemplate>;
    lowerThirdText: string;
}
